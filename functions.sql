
-- Fonction 1
CREATE OR REPLACE FUNCTION getmedscore(oid INTEGER)
RETURNS real AS $$
DECLARE
  nb_notes int;
  moyenne real;
begin
    SELECT count(id_obj), AVG(note)
    INTO nb_notes, moyenne
    FROM user_obj
    WHERE id_obj = oid
    GROUP BY id_obj;

  IF nb_notes < 20 THEN
    RETURN 0;
  ELSE
    RETURN (moyenne);
  END IF;
end;
$$ LANGUAGE plpgsql;

SELECT getmedscore(1);

-- Fonction 2

CREATE OR REPLACE FUNCTION createListeBest(uid INTEGER,otype INTEGER,num INTEGER)
RETURNS void AS $$
DECLARE
  cur_obj  CURSOR FOR SELECT objets.id
                         FROM user_obj JOIN objets on user_obj.id_obj=objets.id
                         WHERE objets.id_type = otype AND user_obj.id_usr = uid
                         order by user_obj.note desc
                         fetch first num rows only;
   id_newListe int;
   objId int;
   cpt int;
begin
    cpt:=1;
    insert into listes (id_type, id_usr, nom, descr) values (otype,uid,'TOP 10','liste of type :'||otype);
    SELECT CURRVAL(pg_get_serial_sequence('listes','id')) INTO id_newListe;
    
    OPEN cur_obj;
    LOOP
        FETCH NEXT FROM cur_obj INTO objId;
        EXIT WHEN NOT FOUND;
        insert into liste_obj (id_liste, id_obj,descr) values (id_newListe,objId,'Top num:'||cpt);
        cpt:=cpt+1;
    END LOOP;
    CLOSE cur_obj;
end;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION bestOf(uid INTEGER)
RETURNS void AS $$
DECLARE
  nb_livres int;
  nb_album int;
  nb_films int;
begin
    SELECT count(user_obj.id_obj)
    INTO nb_livres
    FROM user_obj JOIN objets on user_obj.id_obj=objets.id
    WHERE objets.id_type = 3 AND user_obj.id_usr = uid;

  IF nb_livres >= 10 THEN
    PERFORM createListeBest(uid,3,10);
	RAISE NOTICE 'Top 10 des livres créé';
  END IF;

  SELECT count(user_obj.id_obj)
    INTO nb_album
    FROM user_obj JOIN objets on user_obj.id_obj=objets.id
    WHERE objets.id_type = 1 AND user_obj.id_usr = uid;

  IF nb_album >= 10 THEN
    PERFORM createListeBest(uid,1,10);
	RAISE NOTICE 'Top 10 des albums créé';
  END IF;

  SELECT count(user_obj.id_obj)
    INTO nb_films
    FROM user_obj JOIN objets on user_obj.id_obj=objets.id
    WHERE objets.id_type = 2 AND user_obj.id_usr = uid;

  IF nb_films >= 10 THEN
    PERFORM createListeBest(uid,2,10);
	RAISE NOTICE 'Top 10 des films créé';
  END IF;


end;
$$ LANGUAGE plpgsql;

-- Trigger 1
CREATE OR REPLACE FUNCTION history() RETURNS TRIGGER AS $$
DECLARE
	nouv_nom TEXT;
	c_month TEXT := (SELECT EXTRACT(MONTH from CURRENT_DATE));
	c_year TEXT := (SELECT EXTRACT(YEAR from CURRENT_DATE));
	l_id integer;
BEGIN
	SELECT id INTO l_id FROM listes 
		WHERE id_usr = 1 AND id_type = NEW.id_type AND EXTRACT(MONTH from date_crea) = EXTRACT(MONTH from CURRENT_DATE) and 	EXTRACT(YEAR from date_crea) = EXTRACT(YEAR from CURRENT_DATE);
	IF l_id IS NULL
	THEN
		IF NEW.id_type = 1 THEN nouv_nom := 'nouv_alb_' || c_month || '_' || c_year;
		ELSIF NEW.id_type = 2 THEN nouv_nom := 'nouv_flm_' || c_month || '_' || c_year;
		ELSE nouv_nom := 'nouv_lvr_' || c_month || '_' || c_year;
		END IF;
		INSERT INTO listes (id_type, id_usr, nom) 
			VALUES (NEW.id_type, 1, nouv_nom) RETURNING id INTO l_id;
	END IF;
	INSERT INTO liste_obj(id_liste, id_obj) VALUES (l_id, NEW.id);
	RETURN NEW;
	
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER history_trg AFTER INSERT ON objets
	FOR EACH ROW EXECUTE PROCEDURE history();

	-- Trigger 2 + Test si l'utilisateur possède l'objet qu'il veut commenter 

CREATE OR REPLACE FUNCTION public.concatenation()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
    coment TEXT ;
    idU int;
    idCom int;
BEGIN

   SELECT id_usr INTO idU FROM user_obj
        WHERE id_obj = NEW.id_obj AND id_usr=NEW.id_usr;
	IF idU IS NULL
	THEN
		RAISE EXCEPTION 'Lutilisateur ne possède pas lobjet à commenter'
        USING HINT ='Il faut posséder lobjet';
	END IF;

    SELECT com, id_usr, id INTO coment, idU, idCom FROM commentaires
        ORDER BY date_ajout DESC LIMIT 1;
	IF idU = NEW.id_usr
	THEN
		coment := coment ||' '|| NEW.com;
        UPDATE commentaires SET com = coment 
            WHERE id = idCom;
        RETURN NULL;
	END IF;
	RETURN NEW;
	
END;
$function$


--DROP TRIGGER before_insert_commentaires ON commentaires;

CREATE TRIGGER before_insert_commentaires BEFORE INSERT ON commentaires
	FOR EACH ROW EXECUTE PROCEDURE concatenation();



/* Autres triggers */

CREATE OR REPLACE FUNCTION createObject(t INTEGER) RETURNS INTEGER as $$
DECLARE
	new_id INTEGER;
BEGIN
	INSERT INTO objets(id_type) VALUES (t) RETURNING id into new_id;
	RETURN new_id;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION addAlbum() RETURNS TRIGGER as $$
BEGIN
	IF (NEW.id_obj) IS NULL THEN
		SELECT createObject(1) INTO NEW.id_obj;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION addFilm() RETURNS TRIGGER as $$
BEGIN
	IF (NEW.id_obj) IS NULL THEN
		SELECT createObject(2) INTO NEW.id_obj;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION addLivre() RETURNS TRIGGER as $$
BEGIN
	IF (NEW.id_obj) IS NULL THEN
		SELECT createObject(3) INTO NEW.id_obj;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER newalb_trg BEFORE INSERT ON albums
FOR EACH ROW EXECUTE PROCEDURE addAlbum();

CREATE TRIGGER newfilm_trg BEFORE INSERT ON films
FOR EACH ROW EXECUTE PROCEDURE addFilm();

CREATE TRIGGER newlivre_trg BEFORE INSERT ON livres
FOR EACH ROW EXECUTE PROCEDURE addLivre();


/* Autres fonctions */

CREATE OR REPLACE FUNCTION getalbums(uid INTEGER)
RETURNS TABLE (
	id user_obj.id_usr%TYPE,
	note user_obj.note%TYPE,
	titre albums.titre%TYPE,
	genre albums.genre%TYPE,
	edition albums.date_edit%TYPE
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		o.id_obj as id,
		o.note,
		a.titre,
		a.genre,
		a.date_edit as edition
	FROM
		user_obj o INNER JOIN albums a ON o.id_obj = a.id_obj
	WHERE
		o.id_usr = uid;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getfilms(uid INTEGER)
RETURNS TABLE (
	id user_obj.id_usr%TYPE,
	note user_obj.note%TYPE,
	titre films.titre%TYPE,
	genre films.genre%TYPE,
	sortie films.date_sortie%TYPE
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		o.id_obj as id,
		o.note,
		f.titre,
		f.genre,
		f.date_sortie as sortie
	FROM
		user_obj o INNER JOIN films f ON o.id_obj = f.id_obj
	WHERE
		o.id_usr = uid;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getlivres (uid INTEGER)
RETURNS TABLE (
	id user_obj.id_usr%TYPE,
	note user_obj.note%TYPE,
	titre livres.titre%TYPE,
	genre livres.genre%TYPE,
	style livres."style"%TYPE,
	collection livres.col%TYPE,
	parution livres.date_parution%TYPE
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		o.id_obj as id,
		o.note,
		l.titre,
		l.genre,
		l."style",
		l.col,
		l.date_parution as parution
	FROM
		user_obj o INNER JOIN livres l ON o.id_obj = l.id_obj
	WHERE
		o.id_usr = uid;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_item_coms(oid INTEGER)
RETURNS TABLE (
	id commentaires.id%TYPE,
	auteur TEXT,
	date_ajout commentaires.date_ajout%TYPE,
	commentaire commentaires.com%TYPE
)
AS $$
BEGIN
	RETURN QUERY
	SELECT
		c.id,
		concat(u.prenom, ' ', u.nom) AS auteur,
		c.date_ajout,
		c.com
	FROM
		objets o
		INNER JOIN commentaires c ON o.id = c.id_obj
		INNER JOIN users u ON c.id_usr = u.id
	WHERE
		o.id = oid;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getacteurs(fid INTEGER)
RETURNS TABLE (
	nom personnes.nom%TYPE,
	prenom personnes.prenom%TYPE
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		p.nom,
		p.prenom
	FROM personnes p
		INNER JOIN film_acteur pivot
		ON p.id = pivot.id_pers
	WHERE pivot.id_film = fid;
		
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getreals(fid INTEGER)
RETURNS TABLE (
	nom personnes.nom%TYPE,
	prenom personnes.prenom%TYPE
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		p.nom,
		p.prenom
	FROM personnes p
		INNER JOIN film_real pivot
		ON p.id = pivot.id_pers
	WHERE pivot.id_film = fid;
		
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checktypeliste() RETURNS TRIGGER AS $$
DECLARE 
	type_insert INTEGER;
	type_liste INTEGER;
BEGIN
	SELECT id_type INTO type_insert FROM objets WHERE id = NEW.id_obj;
	SELECT id_type INTO type_liste FROM listes WHERE id = NEW.id_liste;
	IF type_liste <> type_insert
	THEN
		RAISE EXCEPTION 'Le type de lobjet ne correspond pas au type de la liste';
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER liste_type_trg BEFORE INSERT OR UPDATE ON liste_obj
FOR EACH ROW EXECUTE PROCEDURE checktypeliste();