CREATE SEQUENCE usr_seq START WITH 1;
CREATE TABLE users (
    id integer default usr_seq.nextval not null,
    nom varchar2(128) not null,
    prenom varchar2(128) not null,
    login varchar2(255),
    password varchar2(255) not null,
    address varchar2(1024) not null,
    date_inscr date default sysdate not null,
    CONSTRAINT usr_PK PRIMARY KEY id,
    CONSTRAINT pwd_alphanum_CHK CHECK (password
        REGEXP_LIKE(accno, '^[A-Za-z0-9]+$')
    );
);

CREATE SEQUENCE type_seq START WITH 1;
CREATE TABLE types (
    id integer default type_seq.nextval not null,
    nom varchar2(128) not null,
    CONSTRAINT type_PK PRIMARY KEY id,
);

CREATE SEQUENCE obj_seq START WITH 1;
CREATE TABLE objets (
    id integer default obj_seq.nextval not null,
    id_type integer not null,
    CONSTRAINT objet_PK PRIMARY KEY id,
    CONSTRAINT obj_Type_FK FOREIGN KEY (id_type) REFERENCES types(id)
);

CREATE TABLE user_obj (
    id_obj integer not null,
    id_usr integer not null,
    note integer not null,
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id),
    CONSTRAINT id_usr_FK FOREIGN KEY (id_usr) REFERENCES users(id),
    CONSTRAINT CHK_note  CHECK (note<=20 AND note>=0)
);

CREATE TABLE albums (
    id_obj integer not null,
    auteurs varchar2(255) not null,
    genre varchar2(128) not null,
    date_edit date not null,
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id),
    CONSTRAINT CHK_date  CHECK (date_edit < sysdate)
);

CREATE TABLE livres (
    id_obj integer not null,
    auteurs varchar2(255) not null,
    genre varchar2(128) not null,
    style varchar2(128) not null,
    date_paruption date not null,
    col varchar2(128),
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id),
    CONSTRAINT CHK_date  CHECK (date_paruption < sysdate)
);

CREATE TABLE films (
    id_obj integer not null,
    realisateurs varchar2(255) not null,
    acteurs varchar2(255) not null,
    date_sortie date not null,
    genre varchar2(128) not null,
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id),
    CONSTRAINT CHK_date  CHECK (date_sortie < sysdate)
);

CREATE SEQUENCE liste_seq START WITH 1;
CREATE TABLE listes (
    id integer default liste_seq.nextval not null,
    id_type integer not null,
    id_usr integer not null,
    nom varchar2(128) not null,
    descr varchar2(500),
    date_crea date default sysdate not null,
    CONSTRAINT liste_PK PRIMARY KEY id,
    CONSTRAINT id_type_FK FOREIGN KEY (id_type) REFERENCES types(id),
    CONSTRAINT id_usr_FK FOREIGN KEY (id_usr) REFERENCES users(id)
);

CREATE TABLE liste_obj (
    id_liste integer not null,
    id_obj integer not null,
    descr varchar2(500),
    date_ajout date default sysdate not null,
    CONSTRAINT id_liste_FK FOREIGN KEY (id_liste) REFERENCES listes(id),
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id)
);








