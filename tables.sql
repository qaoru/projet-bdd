CREATE TABLE users (
    id serial PRIMARY KEY,
    nom varchar(128) not null,
    prenom varchar(128) not null,
    login varchar(255) UNIQUE not null,
    password varchar(255) not null,
    address varchar(1024) not null,
    date_inscr date default current_date not null,
    CONSTRAINT login_pattern_CHK CHECK (login ~ ('^'|| LOWER(SUBSTRING(prenom, 1, 1)) || LOWER(SUBSTRING(REPLACE(nom, ' ', ''), 1, 7)) ||'[0-9]{2}$')),
    CONSTRAINT pwd_alphanum_CHK CHECK ( password ~* '^[A-Za-z0-9]+$')
);

CREATE TABLE personnes (
    id serial PRIMARY KEY,
    nom VARCHAR(128),
    prenom VARCHAR(128)
);

CREATE TABLE types (
    id serial PRIMARY KEY,
    nom varchar(128) not null
);

CREATE TABLE objets (
    id serial PRIMARY KEY,
    id_type integer not null REFERENCES types(id) ON DELETE CASCADE
);

CREATE TABLE collections (
    id serial PRIMARY KEY,
    nom varchar(255) not null
);

CREATE TABLE user_obj (
    id_obj integer not null,
    id_usr integer not null,
    note integer,
    CONSTRAINT id_obj_FK FOREIGN KEY (id_obj) REFERENCES objets(id) ON DELETE CASCADE,
    CONSTRAINT id_usr_FK FOREIGN KEY (id_usr) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT CHK_note CHECK (note <= 20 AND note > 0)
);
ALTER TABLE user_obj ADD PRIMARY KEY (id_obj, id_usr);

CREATE TABLE albums (
    id_obj integer not null PRIMARY KEY REFERENCES objets(id) ON DELETE CASCADE,
    titre varchar(255) not null,
    genre varchar(128) not null,
    date_edit date default current_date not null,
    CONSTRAINT CHK_date  CHECK (date_edit <= current_date)
);

CREATE TABLE livres (
    id_obj integer not null PRIMARY KEY REFERENCES objets(id) ON DELETE CASCADE,
    titre varchar(255) not null,
    genre varchar(128) not null,
    style varchar(128) not null,
    date_parution date default current_date not null,
    col integer REFERENCES collections(id),
    CONSTRAINT CHK_date CHECK (date_parution <= current_date)
);

CREATE TABLE films (
    id_obj integer not null PRIMARY KEY REFERENCES objets(id) ON DELETE CASCADE,
    titre varchar(255) not null,
    genre varchar(128) not null,
    date_sortie date default current_date not null,
    CONSTRAINT CHK_date CHECK (date_sortie <= current_date)
);

create table film_acteur (
    id_film integer not null REFERENCES films(id_obj) ON DELETE CASCADE,
    id_pers integer not null REFERENCES personnes(id) ON DELETE CASCADE
);
ALTER TABLE film_acteur ADD PRIMARY KEY (id_film, id_pers);

create table film_real (
    id_film integer not null REFERENCES films(id_obj) ON DELETE CASCADE,
    id_pers integer not null REFERENCES personnes(id) ON DELETE CASCADE
);
ALTER TABLE film_real ADD PRIMARY KEY (id_film, id_pers);

create table livre_auteur (
    id_livre integer not null REFERENCES livres(id_obj) ON DELETE CASCADE,
    id_pers integer not null REFERENCES personnes(id) ON DELETE CASCADE
);
ALTER TABLE livre_auteur ADD PRIMARY KEY (id_livre, id_pers);

create table album_auteur (
    id_album integer not null REFERENCES albums(id_obj) ON DELETE CASCADE,
    id_pers integer not null REFERENCES personnes(id) ON DELETE CASCADE
);
ALTER TABLE album_auteur ADD PRIMARY KEY (id_album, id_pers);

CREATE TABLE listes (
    id serial PRIMARY KEY,
    id_type integer not null REFERENCES types(id) ON DELETE CASCADE,
    id_usr integer not null REFERENCES users(id) ON DELETE CASCADE,
    nom varchar(128) not null,
    descr varchar(500),
    date_crea date default current_date not null
); --RAJOUTER VERIF QUE TOUT SOIT LE MEME TYPE

CREATE TABLE liste_obj (
    id_liste integer not null REFERENCES listes(id) ON DELETE CASCADE,
    id_obj integer not null REFERENCES objets(id) ON DELETE CASCADE,
    descr varchar(500),
    date_ajout date default current_date not null
);
ALTER TABLE liste_obj ADD PRIMARY KEY (id_liste, id_obj);

CREATE TABLE commentaires (
	id serial PRIMARY KEY not null,
    id_usr INTEGER not null REFERENCES users(id) ON DELETE CASCADE,
    id_obj INTEGER not null REFERENCES objets(id) ON DELETE CASCADE,
	com text not null,
	date_ajout timestamp default CURRENT_TIMESTAMP not null
);

/* Indexes */

CREATE INDEX ON listes(id_usr);
CREATE INDEX ON user_obj(id_obj, note);
CREATE INDEX ON commentaires(com);
CREATE INDEX ON commentaires(id_obj, id_usr);
CREATE INDEX ON commentaires(date_ajout);