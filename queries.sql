


-- Requête 1 )
-- Les utilisateurs ayant créé une liste pour chacun des types d'objets culturels
-- existant

SELECT users.nom
 FROM (SELECT DISTINCT id_usr,id_type From listes) AS l
    INNER JOIN users ON l.id_usr = users.Id
    GROUP BY l.id_usr, users.nom
    HAVING count(l.id_type)=(select DISTINCT COUNT(*) FROM types);

-- Requête 2 )
-- Les   objets   culturels   ayant   plus   de  20 commentaires
-- et   dont   la   moyenne d'appréciation est supérieure à 14

SELECT pivot.id_obj, nb_coms, avg(note) as moyenne FROM (
		SELECT id_obj, count(com) AS nb_coms FROM commentaires GROUP BY id_obj HAVING COUNT(com) >20
	) AS filtered_coms
	INNER JOIN user_obj pivot
	ON filtered_coms.id_obj = pivot.id_obj
	GROUP BY pivot.id_obj, nb_coms
	HAVING AVG(note) > 14;

-- Requête 3 ) 
-- Les utilisateurs n'ayant jamais mis une note inférieure à 8 
-- à un objet culturel. 

SELECT users.id, users.nom, prenom, min(note)
    FROM users LEFT JOIN user_obj
    ON users.id = user_obj.id_usr
    GROUP BY users.id, users.nom, users.prenom
    HAVING min(note) > 8;

-- Requête 4 )
-- L'objet culturel le plus commenté au cours de la dernière semaine

select id, nbr from (select id_obj as id,count(*) as nbr from commentaires
                                        where 
                                        date_ajout >= (CURRENT_TIMESTAMP - interval '7 day')
                                        AND date_ajout <=  CURRENT_TIMESTAMP
                                        GROUP BY id_obj) as com

where nbr= (SELECT MAX(nbr)from (select id_obj as id,count(*) as nbr from commentaires
                                        where 
                                        date_ajout >= (CURRENT_TIMESTAMP - interval '7 day')
                                        AND date_ajout <=  CURRENT_TIMESTAMP
                                        GROUP BY id_obj) as combis);


-- Autres requêtes)
-- Nécessitent les fonctions

SELECT * from get_item_coms(11);
SELECT * FROM getalbums(4);
SELECT * FROM getfilms(7);
SELECT * FROM getlivres(7);
SELECT * FROM getacteurs(107);
SELECT * FROM getreals(107);

SELECT bestof(13);